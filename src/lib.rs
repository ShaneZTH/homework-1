#[cfg(test)]
mod tests {
    #[test] //compiler derective
    // only relevant in a testing env.
    fn it_works() {
        assert_eq!(2 + 3, 5); //assert is for testing
        
    }
}
